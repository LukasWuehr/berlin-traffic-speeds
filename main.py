import datetime

import urllib3
import json
import sqlite3
from database_handler import Database
from TrafficEyeExplorer import explore_all, Observation

http = urllib3.PoolManager()


def search_iot(*name):
    r = http.request('GET', 'https://api.viz.berlin.de/FROST-Server/v1.1/Things')
    daten = json.loads(r.data)
    value = daten['value']
    useful_ids = []
    for val in value:
        if val['name'] in name:
            useful_ids.append({'name': val['name'], 'thing_id': val['@iot.id']})
    return useful_ids


def get_datastream_kfz(stelle: dict):
    r = http.request('GET', f'https://api.viz.berlin.de/FROST-Server/v1.1/Things({stelle["thing_id"]})/Datastreams')
    daten = json.loads(r.data)
    values = daten['value']
    for value in values:
        if value['name'] == "Kfz-Verkehrsstärke":
            stelle.update({'Q_kfz_id': value['@iot.id'], 'Q_kfz_coordinates': value['observedArea']['coordinates']})
        if value['name'] == "Kfz-Geschwindigkeit":
            stelle.update({'V_kfz_id': value['@iot.id'], 'V_kfz_coordinates': value['observedArea']['coordinates']})
    return stelle


def get_data(stelle: dict, skip=0):
    q = http.request('GET',
                     f'https://api.viz.berlin.de/FROST-Server/v1.1/Datastreams({stelle["Q_kfz_id"]})/Observations?$skip={skip}')
    q_data = json.loads(q.data)
    for value in q_data['value']:
        q_time = value['phenomenonTime']
        q_val = value['result']
        if q_time in stelle['data']:
            stelle['data'][q_time]['Q'] = q_val
        else:
            stelle['data'][q_time] = {'Q': q_val}

    v = http.request('GET',
                     f'https://api.viz.berlin.de/FROST-Server/v1.1/Datastreams({stelle["V_kfz_id"]})/Observations?$skip={skip}')
    v_data = json.loads(v.data)
    for value in v_data['value']:
        v_time = value['phenomenonTime']
        v_val = value['result']
        if v_time in stelle['data']:
            stelle['data'][v_time]['V'] = v_val
        else:
            stelle['data'][v_time] = {'V': v_val}


if __name__ == '__main__':
    #stellen = search_iot('TEU Sensor TEU00129_Det1', 'TEU Sensor TEU00260_Det1')
    #print(stellen)
    #for stelle in stellen:
    #    get_datastream_kfz(stelle)
#
    #print(stellen)
#
    #for stelle in stellen:
    #    stelle['data'] = dict()
    #    for n in range(0, 1000, 100):
    #        get_data(stelle, n)
    #print(stellen)
    #for stelle in stellen:
    #    max_v = 0
    #    max_v_T = None
    #    for time, val in stelle['data'].items():
    #        if 'V' in val and max_v < val['V']:
    #            max_v = val['V']
    #            max_v_T = time
    #    print(max_v, max_v_T)

    #measure_points = explore_all((Observation.V_KFZ, Observation.Q_KFZ))
    db = Database('./messungen.sqlite')
    #for iot_id, values in measure_points.items():
    #    db.set_measure_point(name=values['name'], iot_id=iot_id, coordinates=values['coordinates'], q_id=values[Observation.Q_KFZ]['iot_id'], v_id=values[Observation.V_KFZ]['iot_id'])
    broken = db.find_broken(datetime.datetime.now().replace(day=1, month=1))
    for k, v in broken.items():
        print(k, v)
    db.activate(False, *broken['v_none'].union(broken['q_none']))
    db.run_only_full()
