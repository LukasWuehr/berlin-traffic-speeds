import pandas
from dash import Dash, html, dcc, dash_table
import plotly.express as px
import pandas as pd
from dash.dependencies import Input, Output
from dash.exceptions import PreventUpdate
import sqlite3

global app, map_data
db = 'messungen.sqlite'


def fetch_max_speed(name):
    try:
        df = pd.read_sql_query(f"SELECT MAX(Velocity), Quantity, Time_start FROM {name.replace(' ', '')}",
                               f'sqlite:///{db}')
        return df[df.Quantity != 0]
    except Exception as e:
        print(e)
        raise e


def table_to_df(name):
    try:
        return pd.read_sql_table(name, f'sqlite:///{db}')
    except Exception as e:
        print(e)
        raise e


def get_datapoints_df():
    df = table_to_df("Measure_points")
    # add max speeds
    max_speeds = pd.DataFrame(columns=['MAX(Velocity)', 'Quantity', 'Time_start', 'Name'])
    for _, data in df.iterrows():
        name = data['Name']
        if not data['Active']:
            continue
        max_speed = fetch_max_speed(name)
        max_speed['Name'] = [name] * len(max_speed)
        max_speeds = pd.concat([max_speed, max_speeds], ignore_index=True)

    max_speeds = max_speeds[max_speeds['Name'].notnull()]
    df = df.merge(max_speeds, on=['Name', 'Name'], how='outer')
    df["Quantity"] = pd.to_numeric(df["Quantity"])
    df["MAX(Velocity)"] = pd.to_numeric(df["MAX(Velocity)"])
    df["Time_start"] = pd.to_datetime(df["Time_start"])
    df.loc[df["Quantity"].isnull(), "Quantity"] = 0.1
    df.loc[df["MAX(Velocity)"].isnull(), "MAX(Velocity)"] = 0.1
    return df


def set_map():
    df = get_datapoints_df()
    fig = px.scatter_mapbox(df, lat='Y_pos', lon='X_pos', mapbox_style='stamen-terrain',
                            size='MAX(Velocity)', color='Quantity', color_continuous_scale=px.colors.sequential.Plasma,
                            hover_data=['Name', 'IotID'])  # stamen-toner
    return fig


if __name__ == '__main__':
    app = Dash(__name__)

    app.layout = html.Div(children=[
        html.H1(children='Berlin Traffic Speeds'),

        html.Div(children='''
            A web application to view vehicle speeds in Berlin
        '''),

        dcc.RadioItems(['speed', 'quantity'], 'speed', inline=True, id='map-radio'),
        dcc.Graph(id='map', figure=set_map()),
        dash_table.DataTable(id='map-data', sort_action='native',
                             data=get_datapoints_df().to_dict('records'),
                             columns=[{"name": i, "id": i} for i in get_datapoints_df().columns])
    ])

    app.run_server(debug=True)
