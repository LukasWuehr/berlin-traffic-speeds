import enum

import urllib3
import json
from enum import Enum


class Observation(Enum):
    Q_KFZ = 'Kfz-Verkehrsstärke'
    V_KFZ = 'Kfz-Geschwindigkeit'
    Q_PKW = 'Pkw-Verkehrsstärke'
    V_PKW = 'Pkw-Geschwindigkeit'
    Q_LKW = 'Lkw-Verkehrsstärke'
    V_LKW = 'Lkw-Geschwindigkeit'


def explore_TEUs(*names):
    http = urllib3.PoolManager()
    teus = dict()
    if names:
        query = "' or name eq '".join(names)
        skip = 0
        while skip < 1000:
            r = http.request('GET',
                             f"https://api.viz.berlin.de/FROST-Server/v1.1/Things?$filter=name eq '{query}'&$skip={skip}")
            skip += 100
            daten = json.loads(r.data)
            value = daten['value']
            if len(value) == 0:
                break
            for val in value:
                if val['name'] in names:
                    teus[val['@iot.id']] = val['name']
        print('found ', len(names), '/', len(teus), 'Traffic Eyes')

    else:
        skip = 0
        while skip < 1000:
            r = http.request('GET',
                             f"https://api.viz.berlin.de/FROST-Server/v1.1/Things?$skip={skip}")
            skip += 100
            daten = json.loads(r.data)
            value = daten['value']
            if len(value) == 0:
                break
            for val in value:
                teus[val['@iot.id']] = val['name']
        print('found ', len(teus), 'Traffic Eyes')
    return teus


def explore_datastreams_5min(iot_id, *observations: Observation):
    http = urllib3.PoolManager()
    r = http.request("GET", f"https://api.viz.berlin.de/FROST-Server/v1.1/Things({iot_id})/Datastreams")
    data = json.loads(r.data)
    values = data['value']

    datastreams = dict()
    if not observations:
        observations = Observation
    observations = {x.value: x for x in observations}
    for value in values:
        if value['name'] in observations:
            datastreams[observations[value['name']]] = {'iot_id': value['@iot.id']}
    return datastreams


def explore_all(observations=None, *eyes):
    teus = explore_TEUs(*eyes)
    explored = dict()
    for teu_id, name in teus.items():
        datastreams = explore_datastreams_5min(teu_id, *observations)
        loc = get_location(teu_id)
        explored[teu_id] = {'name': name, 'coordinates': loc}
        explored[teu_id].update({stream: value for stream, value in datastreams.items()})
    print('explored', len(explored), "Things")
    return explored


def explore_region(polygon):
    pass


def get_location(iot_id):
    http = urllib3.PoolManager()
    r = http.request('GET', f'https://api.viz.berlin.de/FROST-Server/v1.1/Things({iot_id})/Locations')
    data = json.loads(r.data)
    values = data['value']
    return values[0]['location']['coordinates']


if __name__ == '__main__':
    teus = explore_all((Observation.V_KFZ, Observation.Q_KFZ), 'TEU Sensor TEU00379_Det0', 'TEU Sensor TEU00129_Det1')
    print(teus)