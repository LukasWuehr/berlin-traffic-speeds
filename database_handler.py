import concurrent.futures
import datetime
import json
import re
import typing
import urllib3
import sqlite3
import os
from time import sleep
from threading import Lock


def get_datastream(iot_id, offset=0):
    pool = urllib3.PoolManager()
    req = pool.request('GET',
                       f'https://api.viz.berlin.de/FROST-Server/v1.1/Datastreams({iot_id})/Observations?$skip={offset}&$orderby=phenomenonTime desc')
    value_map = dict()

    jason = json.loads(req.data)

    for value in jason['value']:
        time = value['phenomenonTime']
        val = value['result']
        start, end = time.split('/')
        start = datetime.datetime.strptime(start, '%Y-%m-%dT%X.000Z')
        end = datetime.datetime.strptime(end, '%Y-%m-%dT%X.000Z')
        value_map[start] = {'value': val, 'end': end}

    return value_map


class Database:
    def __init__(self, path, *measure_points):
        """
        load the Database and Tables with Measure points

        :param path: path to sqlite database
        :param measure_points: (Optional) set measure points
        """
        self.lock = Lock()
        if not os.path.isfile(path):
            open(path, 'x').close()
        self.path = path

        if not self.fetchone(
                "SELECT sqlite_master.tbl_name as name FROM sqlite_master where name='Measure_points';"):
            self.commit('CREATE TABLE Measure_points ('
                        'IotID int NOT NULL ,'
                        'Name varchar(255),'
                        'X_pos float,'
                        'Y_pos float,'
                        'Q_ID int,'
                        'V_ID int,'
                        'Active boolean default TRUE, '
                        'PRIMARY KEY (IotID));')
        query = self.fetchall('SELECT IotID FROM Measure_points')
        self.measure_points = [x[0] for x in query] if query is not None else []

        if measure_points:
            self.set_measure_points(*measure_points)

    def fetchone(self, sql, arg=()):
        conn = sqlite3.connect(self.path)
        try:
            fetch = conn.execute(sql, arg).fetchone()
        except Exception as e:
            conn.rollback()
            print(e, sql, arg)
            raise e
        finally:
            conn.close()
        return fetch

    def fetchall(self, sql, arg=()):
        conn = sqlite3.connect(self.path)
        try:
            fetch = conn.execute(sql, arg).fetchall()
        except Exception as e:
            conn.rollback()
            print(e, sql, arg)
            raise e
        finally:
            conn.close()
        return fetch

    def commit(self, sql, arg=()):
        conn = sqlite3.connect(self.path)
        try:
            conn.execute(sql, arg)
            conn.commit()
        except Exception as e:
            conn.rollback()
            print(e, sql, arg)
            raise e
        finally:
            conn.close()

    def set_measure_points(self, *points):
        for point in points:
            if isinstance(point, dict):
                self.set_measure_point(point['name'], point['coordinates'], point['iot_id'], q_id=point['q_id'],
                                       v_id=point['v_id'])
            if isinstance(point, (list, tuple)):
                self.set_measure_point(*point)

    def set_measure_point(self, name: str, coordinates: typing.Iterable[float], iot_id: int, **iot_ids):
        if self.fetchone(f"SELECT * FROM Measure_points WHERE IotID={iot_id}"):
            return
        self.commit(
            f"INSERT INTO Measure_points (IotID, Name, X_pos, Y_pos) VALUES ({iot_id}, '{name}', {coordinates[0]}, {coordinates[1]})")
        self.commit(f"CREATE TABLE {name.replace(' ', '')} ("
                    f"ID integer NOT NULL PRIMARY KEY AUTOINCREMENT,"
                    f"Quantity int,"
                    f"Velocity int,"
                    f"Time_start datetime,"
                    f"Time_end datetime);")
        self.measure_points.append(iot_id)
        if 'q_id' in iot_ids:
            self.set_Q_ID(iot_id, iot_ids['q_id'])
        if 'v_id' in iot_ids:
            self.set_V_ID(iot_id, iot_ids['v_id'])

    def set_Q_ID(self, iot_id: int, q_id: int):
        """
        set Datastream id for Quantity of KFZ/5min in Measure_points Table.

        :param iot_id: ID of Measure point (Thing)
        :param q_id: ID of Datastream
        """
        self.commit(f"UPDATE Measure_points "
                    f"SET Q_ID = {q_id} "
                    f"WHERE IotID = {iot_id};")

    def set_V_ID(self, iot_id, v_id):
        """
        set Datastream id for Speed of KFZ/5min in Measure_points Table.

        :param iot_id: ID of Measure point (Thing)
        :param v_id: ID of Datastream
        """
        self.commit(f"UPDATE Measure_points "
                    f"SET V_ID = {v_id} "
                    f"WHERE IotID = {iot_id};")

    def remove_from_updating_by_name(self, *names):
        query = self.fetchall(f"SELECT IotID FROM Measure_points WHERE Name in ({','.join(names)})")
        if query:
            self.remove_from_updating(*[x[0] for x in query])

    def remove_from_updating(self, *iot_ids):
        for iot_id in iot_ids:
            try:
                self.measure_points.remove(iot_id)
                print('removed', iot_id)
            except ValueError:
                print('No such active id', iot_id)

    def run_only_full(self):
        q = self.fetchall("SELECT IotID FROM Measure_points WHERE Q_ID IS NULL OR V_ID IS NULL or Active = FALSE")
        if q:
            print('deactivate entries without Q_ID or V_ID')
            self.remove_from_updating(*[x[0] for x in q])
        self.run()

    def run(self):
        """
        save all from set datastreams of Measure_points. Start at last saved date. If no date or too far in the past use now.
        """

        print('Start Updating...')
        print('Use Measure Points: ')
        string_points = re.sub(r'[\[\]]', '', str(self.measure_points))
        active_points = self.fetchall(f"SELECT * FROM Measure_points WHERE IotID IN ({string_points})")
        for point in active_points:
            print(point)

        with concurrent.futures.ThreadPoolExecutor() as executer:
            while True:
                executer.map(self.update_table, active_points)
                print('UPDATE CYCLE DONE! wait 29700sec\n\n')
                sleep(29700)

    def update_table(self, point):

        _, name, _, _, q_id, v_id, _ = point
        name = name.replace(' ', '')
        last_time = self.fetchone(
            f"SELECT Time_start "
            f"FROM {name} "
            f"WHERE Velocity IS NOT NULL AND Quantity IS NOT NULL "
            f"ORDER BY Time_start DESC"
        )
        if not last_time:
            last_time = datetime.datetime.now()
            last_time = last_time.replace(second=0, microsecond=0, minute=last_time.minute - last_time.minute % 5)
        else:
            last_time = datetime.datetime.fromisoformat(last_time[0])

        print(f'Update {name} at {datetime.datetime.now()}')

        with concurrent.futures.ThreadPoolExecutor() as pool:
            pool.map(self.update_data, (q_id, v_id), ('Quantity', 'Velocity'), (name, name), (last_time, last_time))
        print(f'{name} done update at {datetime.datetime.now()}')

    def update_data(self, iot_id, measurement, table_name, last_time):
        for skip in range(0, 9000, 100):
            data = get_datastream(iot_id=iot_id, offset=skip)
            if len(data) == 0:
                return
            for values in data.items():
                self.insert_data(table_name, measurement, values)
            if last_time in data:
                return

    def insert_data(self, table_name, measurement, data):

        conn = sqlite3.connect(self.path)
        try:
            self.lock.acquire()
            if conn.execute(f"SELECT * FROM {table_name} WHERE Time_start = ?", (data[0],)).fetchone():
                conn.execute(f"UPDATE {table_name} SET {measurement}= ? WHERE Time_start= ?",
                             (data[1]['value'], data[0]))
            else:
                conn.execute(f"INSERT INTO {table_name} ({measurement}, Time_start, Time_end) "
                             f"VALUES (?,?,?)", (data[1]['value'], data[0], data[1]['end']))
            conn.commit()
        except Exception as e:
            conn.rollback()
            print(e, table_name, measurement, data)
            raise e
        finally:
            conn.close()
            self.lock.release()

    def find_broken(self, min_time=datetime.datetime.min):
        broken = {'old': set(), 'empty': set(), 'v_none': set(), 'q_none': set(), 'v_0': set(), 'q_0': set()}
        for name, iot_id in self.fetchall('SELECT Name, IotID FROM Measure_points'):
            table = name.replace(' ', '')
            if not self.fetchone(f'SELECT * FROM {table}'):
                broken['empty'].add(iot_id)
                continue
            if not self.fetchone(f'SELECT Time_start FROM {table} WHERE Time_start > ? ORDER BY Time_start desc ',
                                 (min_time,)):
                broken['old'].add(iot_id)

            v = self.fetchone(f'SELECT Velocity FROM {table} ORDER BY Velocity desc')[0]
            if not v:
                broken['v_none'].add(iot_id)
            elif v == 0:
                broken['v_0'].add(iot_id)

            q = self.fetchone(f'SELECT Quantity FROM {table} ORDER BY Quantity desc')[0]
            if not q:
                broken['q_none'].add(iot_id)
            elif q == 0:
                broken['q_0'].add(iot_id)

        return broken

    def activate(self, active=True, *iot_ids):
        for iot in iot_ids:
            self.commit('UPDATE Measure_points SET Active = ? WHERE IotID = ?', (active, iot))
